package com.example.fidelitybe.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.sql.Timestamp;

@Entity
@Table(name = "punti")
@Getter
@Setter
public class Punti {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "ts")
    private Timestamp ts;

    @Column(name = "importo")
    private double importo;

    @Column(name = "note")
    private String note;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id_fidelity", referencedColumnName = "id")
    @JsonIgnoreProperties(value = {"idUtente", "id_fidelity"})
    private FidelityCard id_fidelity;

}
