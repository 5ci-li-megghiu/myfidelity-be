package com.example.fidelitybe.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

@Entity
@Getter
@Setter
@Table(name = "fidelity_card")
public class FidelityCard {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "data_inizio")
    private Timestamp dataInizio;

    @Column(name = "data_fine")
    private Timestamp dataFine;

    @Column(name = "tot_punti")
    private int totPunti;

    @Column(name = "attiva")
    private boolean attiva;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id_utente")
    @JsonIgnore
    private Utente id_utente;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "id_fidelity")
    @JsonIgnoreProperties(value = {"id_fidelity"})
    private Set<Punti> punti = new HashSet<>();

}
