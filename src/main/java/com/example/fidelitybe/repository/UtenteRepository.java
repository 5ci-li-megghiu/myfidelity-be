package com.example.fidelitybe.repository;

import com.example.fidelitybe.domain.Utente;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UtenteRepository extends JpaRepository<Utente, Long> {
    Utente findUtenteById(Long id);

    Optional<Utente> findUtenteByUsername(String username);

    boolean existsByUsername(String username);

    boolean existsByCodice(String codice);

    Optional<Utente> findUtenteByCodice(String codice);
}
