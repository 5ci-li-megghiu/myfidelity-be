package com.example.fidelitybe.repository;

import com.example.fidelitybe.domain.FidelityCard;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@EnableJpaRepositories
@Repository
public interface FidelityCardRepository extends JpaRepository<FidelityCard, Long> {
    Optional<FidelityCard> findFidelityCardById(Long id);


    @Query(nativeQuery = true, value = "SELECT * FROM fidelity_card WHERE id_utente=?1 AND attiva is TRUE")
    Optional<FidelityCard> findActiveFidelityByIdUtente(Long id);

    @Query(nativeQuery = true, value = "SELECT * FROM fidelity_card WHERE id_utente=?1")
    Optional<List<FidelityCard>> findAllByIdUtente(Long id);
}
