package com.example.fidelitybe.controller;

import com.example.fidelitybe.controller.dto.PointDTO;
import com.example.fidelitybe.controller.dto.SetTotalPointFidelityManuallyDTO;
import com.example.fidelitybe.domain.FidelityCard;
import com.example.fidelitybe.service.FidelityCardService;
import com.example.fidelitybe.service.PuntiService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api")
@AllArgsConstructor
public class FidelityCardController {
    private final FidelityCardService fidelityCardService;

    private final PuntiService puntiService;

    @GetMapping("/all-fidelity/{user-id}")
    public ResponseEntity<List<FidelityCard>> getFidelityCardsByUserId(@PathVariable(value = "user-id") Long userId) {
        List<FidelityCard> fidelityCardList = fidelityCardService.fidelityCardByUserId(userId);

        return ResponseEntity.ok().body(fidelityCardList);
    }


    @GetMapping("/fidelity/{user-id}")
    public ResponseEntity<FidelityCard> getActiveFidelityCardsByUserId(@PathVariable(value = "user-id") Long userId) {
        FidelityCard fidelityCardList = fidelityCardService.activeFidelityCardByUserId(userId);
        return ResponseEntity.ok().body(fidelityCardList);
    }

    @PostMapping("/set-point-manually")
    public ResponseEntity<FidelityCard> setPointToFidelityByAdminManually(@RequestBody SetTotalPointFidelityManuallyDTO setPointManuallyDTO) {
        FidelityCard fidelityCard = fidelityCardService.setPointToFidelityByAdminManually(setPointManuallyDTO);
        return ResponseEntity.ok(fidelityCard);
    }

    @GetMapping("/point/{codice}")
    public ResponseEntity<PointDTO> getPuntiByCodice(@PathVariable(value = "codice") String codice) {
        return ResponseEntity.ok(puntiService.getPuntiByCodice(codice));
    }

    @GetMapping("/complete-fidelity/{codice}")
    public ResponseEntity<FidelityCard> completeFidelityCard(@PathVariable(value = "codice") String codice) {

        return ResponseEntity.ok(fidelityCardService.completeFidelityCard(codice));
    }
}
