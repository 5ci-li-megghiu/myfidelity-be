package com.example.fidelitybe.controller;

import com.example.fidelitybe.controller.dto.AddPointDTO;
import com.example.fidelitybe.controller.dto.CommonResponseMessage;
import com.example.fidelitybe.domain.Punti;
import com.example.fidelitybe.service.PuntiService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/api/punti")
@AllArgsConstructor
public class PuntiController {
    private final PuntiService puntiService;

    @PostMapping("/add-point")
    public ResponseEntity<CommonResponseMessage> addPoint(@RequestBody AddPointDTO setPointManuallyDTO) {
        Punti punti = puntiService.addPoint(setPointManuallyDTO);
        return ResponseEntity.ok(new CommonResponseMessage("punti modificati correttamente", 0));
    }
}
