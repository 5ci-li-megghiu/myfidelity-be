package com.example.fidelitybe.controller.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class AddPointDTO implements Serializable {
    private String codice;
    private int punti;
    private double spesa;
}
