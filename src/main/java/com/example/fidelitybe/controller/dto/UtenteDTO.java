package com.example.fidelitybe.controller.dto;

import com.example.fidelitybe.enums.RoleEnum;
import lombok.*;

import java.io.Serializable;

@Data
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UtenteDTO implements Serializable {
    private Long id;
    private String nome;
    private String cognome;
    private String email;
    private String data;
    private RoleEnum ruolo;
    private String codice;
    private String cellulare;
    private String username;
}
