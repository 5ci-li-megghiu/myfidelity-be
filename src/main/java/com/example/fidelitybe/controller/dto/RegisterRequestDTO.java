package com.example.fidelitybe.controller.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Data
@AllArgsConstructor
@Getter
@Setter
public class RegisterRequestDTO implements Serializable {
    private String username;
    private String password;
    private String nome;
    private String cognome;
    private String email;
    private String data;
    private String cellulare;
}
