package com.example.fidelitybe.controller.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class SetTotalPointFidelityManuallyDTO implements Serializable {
    private int punti;
    private Long id;
}
