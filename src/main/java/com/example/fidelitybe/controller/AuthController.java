package com.example.fidelitybe.controller;

import com.example.fidelitybe.controller.dto.*;
import com.example.fidelitybe.repository.UtenteRepository;
import com.example.fidelitybe.service.AuthService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@Controller
@RequestMapping("/api/auth")
@AllArgsConstructor
public class AuthController {

    private final AuthService authService;

    private final UtenteRepository utenteRepository;

    @PostMapping("/register")
    public ResponseEntity<RegisterResponseDTO> register(@RequestBody RegisterRequestDTO registerRequestDto) {
        if (utenteRepository.existsByUsername(registerRequestDto.getUsername())) {
            throw new ResponseStatusException(
                    HttpStatus.CONFLICT, "entity not found"
            );
        }
        
        return ResponseEntity.ok(authService.register(registerRequestDto));
    }

    @PostMapping("/login")
    public ResponseEntity<LoginResponseDTO> login(@RequestBody LoginRequestDTO loginRequest) {
        return ResponseEntity.ok(authService.login(loginRequest));
    }
}
