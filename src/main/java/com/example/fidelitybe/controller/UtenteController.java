package com.example.fidelitybe.controller;

import com.example.fidelitybe.controller.dto.UtenteDTO;
import com.example.fidelitybe.domain.Utente;
import com.example.fidelitybe.service.FidelityCardService;
import com.example.fidelitybe.service.UtenteService;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.logging.LogManager;

@Controller
@RequestMapping("/api")
@CrossOrigin
public class UtenteController {

    private final UtenteService utenteService;

    UtenteController(UtenteService utenteService) {
        this.utenteService = utenteService;
    }

    @GetMapping("/user/{id}")
    public ResponseEntity<UtenteDTO> getUtenteById(@PathVariable(name = "id") Long id) {
        Utente utente = utenteService.findUtenteById(id);

        return ResponseEntity.ok().body(new UtenteDTO(utente.getId(), utente.getNome(), utente.getCognome(), utente.getEmail(), utente.getData(), utente.getRuolo(), utente.getCodice(), utente.getCellulare(), utente.getUsername()));
    }

    @GetMapping("/user-list")
    public ResponseEntity<List<UtenteDTO>> getUtenteById() {
        List<Utente> utenti = utenteService.findAllUtenti();

        return ResponseEntity.ok().body(utenti.stream().map((utente) -> new UtenteDTO(utente.getId(), utente.getNome(), utente.getCognome(), utente.getEmail(), utente.getData(), utente.getRuolo(), utente.getCodice(), utente.getCellulare(), utente.getUsername())).toList());
    }
}

