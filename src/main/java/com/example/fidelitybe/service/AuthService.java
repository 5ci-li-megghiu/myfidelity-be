package com.example.fidelitybe.service;

import com.example.fidelitybe.config.JwtService;
import com.example.fidelitybe.controller.dto.*;
import com.example.fidelitybe.domain.FidelityCard;
import com.example.fidelitybe.domain.Utente;
import com.example.fidelitybe.enums.RoleEnum;
import com.example.fidelitybe.repository.FidelityCardRepository;
import com.example.fidelitybe.repository.UtenteRepository;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.extern.apachecommons.CommonsLog;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Optional;

@Service
@AllArgsConstructor
public class AuthService {

    private final UtenteRepository utenteRepository;

    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    private final JwtService jwtService;

    private final AuthenticationManager authenticationManager;

    private final FidelityCardRepository fidelityCardRepository;

    public RegisterResponseDTO register(
            RegisterRequestDTO registerRequestDto
    ) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

        Date now = new Date();

        Utente utente = new Utente();

        utente.setUsername(registerRequestDto.getUsername());
        utente.setPassword(bCryptPasswordEncoder.encode(registerRequestDto.getPassword()));
        utente.setCellulare(registerRequestDto.getCellulare());
        utente.setCodice(generateCodice());
        utente.setData(formatter.format(now));
        utente.setNome(registerRequestDto.getNome());
        utente.setCognome(registerRequestDto.getCognome());
        utente.setEmail(registerRequestDto.getEmail());
        utente.setRuolo(RoleEnum.USER);

        String jwtToken = jwtService.generateToken(utente);

        utenteRepository.save(utente);

        FidelityCard fidelityCard = new FidelityCard();
        fidelityCard.setId_utente(utente);
        fidelityCard.setTotPunti(0);
        fidelityCard.setDataInizio(new Timestamp(System.currentTimeMillis()));
        fidelityCard.setAttiva(true);

        fidelityCardRepository.save(fidelityCard);

        return RegisterResponseDTO.builder().
                token(jwtToken).
                data(new UtenteDTO(
                        utente.getId(),
                        utente.getNome(),
                        utente.getCognome(),
                        utente.getEmail(),
                        utente.getData(),
                        utente.getRuolo(),
                        utente.getCodice(),
                        utente.getCellulare(),
                        utente.getUsername()))
                .build();
    }

    public LoginResponseDTO login(LoginRequestDTO loginRequest) {
        authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));

        Optional<Utente> optUtente = utenteRepository.findUtenteByUsername(loginRequest.getUsername());
        if (optUtente.isEmpty()) {
            return new LoginResponseDTO("", 404, new UtenteDTO());
        }

        String jwtToken = jwtService.generateToken(optUtente.get());

        Utente utente = optUtente.get();

        return new LoginResponseDTO(jwtToken, 200, new UtenteDTO(
                utente.getId(),
                utente.getNome(),
                utente.getCognome(),
                utente.getEmail(),
                utente.getData(),
                utente.getRuolo(),
                utente.getCodice(),
                utente.getCellulare(),
                utente.getUsername())
        );
    }

    private String generateCodice() {
        String codice;
        do {
            codice = RandomStringUtils.randomAlphabetic(7);
        } while (utenteRepository.existsByCodice(codice));
        return codice;
    }
}
