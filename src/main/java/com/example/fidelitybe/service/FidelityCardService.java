package com.example.fidelitybe.service;

import com.example.fidelitybe.controller.dto.SetTotalPointFidelityManuallyDTO;
import com.example.fidelitybe.domain.FidelityCard;
import com.example.fidelitybe.domain.Punti;
import com.example.fidelitybe.domain.Utente;
import com.example.fidelitybe.repository.FidelityCardRepository;
import com.example.fidelitybe.repository.PuntiRepository;
import com.example.fidelitybe.repository.UtenteRepository;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.sql.Timestamp;
import java.util.List;

@Service
public class FidelityCardService {
    private final FidelityCardRepository fidelityCardRepository;

    private final PuntiRepository puntiRepository;
    private final UtenteRepository utenteRepository;

    public FidelityCardService(FidelityCardRepository fidelityCardRepository, PuntiRepository puntiRepository, UtenteRepository utenteRepository) {
        this.fidelityCardRepository = fidelityCardRepository;
        this.puntiRepository = puntiRepository;
        this.utenteRepository = utenteRepository;
    }

    public FidelityCard findFidelityCardById(Long id) {
        FidelityCard fidelityCard = fidelityCardRepository.findFidelityCardById(id).orElseThrow();

        return fidelityCard;
    }


    public List<FidelityCard> fidelityCardByUserId(Long id) {
        List<FidelityCard> fidelityCard = fidelityCardRepository.findAllByIdUtente(id).orElseThrow();

        return fidelityCard;
    }

    public FidelityCard activeFidelityCardByUserId(Long id) {
        FidelityCard fidelityCard = fidelityCardRepository.findActiveFidelityByIdUtente(id).orElseThrow();

        return fidelityCard;
    }

    public FidelityCard setPointToFidelityByAdminManually(SetTotalPointFidelityManuallyDTO setPointManuallyDTO) {
        if (setPointManuallyDTO.getPunti() > 12) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "bad request");
        }
        FidelityCard fidelityCard = fidelityCardRepository.findActiveFidelityByIdUtente(setPointManuallyDTO.getId()).orElseThrow();

        int pointDifference = setPointManuallyDTO.getPunti() - fidelityCard.getTotPunti();

        Punti punti = new Punti();
        punti.setImporto(0.0);
        punti.setNote(pointDifference + " punti settati da admin");
        punti.setId_fidelity(fidelityCard);
        punti.setTs(new Timestamp(System.currentTimeMillis()));


        if (setPointManuallyDTO.getPunti() == 12) {
            fidelityCard.setDataFine(new Timestamp(System.currentTimeMillis()));
        }
        fidelityCard.setTotPunti(setPointManuallyDTO.getPunti());

        puntiRepository.save(punti);
        fidelityCardRepository.save(fidelityCard);
        return fidelityCard;
    }

    public FidelityCard completeFidelityCard(String codice) {
        Utente utente = utenteRepository.findUtenteByCodice(codice).orElseThrow();

        FidelityCard oldFidelityCard = fidelityCardRepository.findActiveFidelityByIdUtente(utente.getId()).orElseThrow();
        oldFidelityCard.setDataFine(new Timestamp(System.currentTimeMillis()));
        oldFidelityCard.setAttiva(false);

        FidelityCard newFidelityCard = new FidelityCard();

        newFidelityCard.setId_utente(utente);
        newFidelityCard.setDataInizio(new Timestamp(System.currentTimeMillis()));
        newFidelityCard.setAttiva(true);

        fidelityCardRepository.save(newFidelityCard);
        fidelityCardRepository.save(oldFidelityCard);

        return newFidelityCard;
    }
}
