package com.example.fidelitybe.service;

import com.example.fidelitybe.controller.dto.AddPointDTO;
import com.example.fidelitybe.controller.dto.PointDTO;
import com.example.fidelitybe.domain.FidelityCard;
import com.example.fidelitybe.domain.Punti;
import com.example.fidelitybe.domain.Utente;
import com.example.fidelitybe.repository.FidelityCardRepository;
import com.example.fidelitybe.repository.PuntiRepository;
import com.example.fidelitybe.repository.UtenteRepository;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.sql.Timestamp;

@Service
@AllArgsConstructor
public class PuntiService {
    private final PuntiRepository puntiRepository;

    private final UtenteRepository utenteRepository;

    private final FidelityCardRepository fidelityCardRepository;

    public Punti addPoint(AddPointDTO addPointDTO) {

        Utente utente = utenteRepository.findUtenteByCodice(addPointDTO.getCodice())
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "nessun utente trovato per questa codice fidelity"));

        Long id = utente.getId();

        FidelityCard fidelityCardActive = fidelityCardRepository.findActiveFidelityByIdUtente(utente.getId()).
                orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "nessuna fidelity trovata per quest'utente"));

        fidelityCardActive.setTotPunti(fidelityCardActive.getTotPunti() + 1);
        Punti punti = new Punti();
        punti.setImporto(addPointDTO.getSpesa());
        punti.setId_fidelity(fidelityCardActive);
        punti.setTs(new Timestamp(System.currentTimeMillis()));
        punti.setNote("");

        puntiRepository.save(punti);
        return punti;
    }

    public PointDTO getPuntiByCodice(String codice) {
        Utente utente = utenteRepository.findUtenteByCodice(codice).orElseThrow();

        FidelityCard fidelityCardActive = fidelityCardRepository.findActiveFidelityByIdUtente(utente.getId()).orElseThrow();

        return new PointDTO(fidelityCardActive.getTotPunti());
    }
}
