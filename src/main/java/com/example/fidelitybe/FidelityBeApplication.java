package com.example.fidelitybe;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;

@SpringBootApplication()
public class FidelityBeApplication {

    public static void main(String[] args) {
        SpringApplication.run(FidelityBeApplication.class, args);
    }

}
